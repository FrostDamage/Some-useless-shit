<?php

/****
 * TODO: Refactor everything
 ****/

class Sted_MobileModelProgressMedia extends Sted_MobileModel
{
  function getProgressImage($type,$chid) {
  	$progress_images = '1';
    $query = DBHelper::zQuery(true)
    ->select($type)
    ->from('#__sted_notes_newimages')
    ->where([
      ['type', '=', '1'],
      ['chid', '=', $chid],
    ]);
    DBHelper::setQuery($query);
    $notes_images = DBHelper::loadResult();
    $progress_images = unserialize($notes_images);
    return $progress_images;
  }

  function getWellbeingImages($type,$chid) {
  	$progress_images = '1';
    $query = DBHelper::setQuery(true)
    ->select($type)
    ->from('#__sted_notes_newimages')
    ->where([
      ['type', '=', '1'],
      ['chid', '=', $chid],
    ]);
    $notes_images = DBHelper::loadResult();
    DBHelper::setQuery($query);
    $progress_images = unserialize($notes_images);
    return $progress_images;
  }

  function getFirstNewprogressImage($type,$chid,$data){
    $query = DBHelper::setQuery(true)
    ->select('( imagename )')
    ->from('#__sted_3rd_plugin_image_gallery_newprogress')
    ->where([
      ['type', '=', $type],
      ['chid', '=', $chid],
      ['rowid', '=', $data],
    ])
    ->order('uploaddate', DESC)
    ->setLimit('1');
    DBHelper::setQuery($query);
    $images = DBHelper::loadResult();

    return $images;
  }

  /**
   *
   *
   * @param type $type
   * @param type $chid
   * @return id_video
   *
   * @edit 19.02.2016 sergey Bug #3784
   */
  function getFirstNewprogressVideo($type,$chid,$data){
    $query = DBHelper::setQuery(true)
    ->select('area_of_learning')
    ->from('#__sted_3rd_plugin_video_gallery_newprogress')
    ->where([
      ['chid', '=', $chid],
      ['type', '=', $type],
      ['rowid', '=', $data],
    ]);
    DBHelper::setQuery($query);
    $images = DBHelper::loadResult();

    return $images;
  }

  /* IMAGE */
  /**
   * @edit 1.0.7 15.02.2016 Sergey Dev #3535 retern Add parameter type
   */
  function getImageGalleryChild($chid,$type,$tab,$item){
    $query = DBHelper::setQuery(true)
    ->select('id', 'aid', 'chid', 'uploaddate', 'imagename', 'title', 'imgdesc', 'type')
    ->from('#__sted_3rd_plugin_image_gallery_newprogress')
    ->where([
      ['chid', '=', $chid],
      ['type', '=', $type],
      ['tab', '=', $tab],
      ['rowid', '=', $item],
    ])
    ->order('uploaddate', DESC);
    DBHelper::setQuery($query);
    $images = DBHelper::loadObjectList();

    return $images;
  }

  /* VIDEO */
  function getVideoGalleryChild($chid,$type,$tab,$item){
    //$query='SELECT tab,rowid,imagename FROM jos_sted_3rd_plugin_image_gallery_newprogress WHERE chid='.$chid.' and type="'.$type.'" ';
    $query = DBHelper::setQuery(true)
    ->select('vgn.id',
             'vgn.aid',
             'vgn.video_id',
             'vgn.dateuploaded',
             'year(vgn.dateuploaded) as year',
             'month(vgn.dateuploaded) as month',
             'day(vgn.dateuploaded) as day',
             'vgn.name',
             'vgn.title',
             'vgn.videodesc',
             'vgn.type',
             'vgn.tab',
             'vgn.rowid',

             'vg.duration',
             'vg.screenshot',
             'vg.status',

             'admin.first_name as admins_first_name',
             'admin.last_name as admins_last_name')
    ->from('#__sted_3rd_plugin_video_gallery_newprogress as vgn')
    ->join('LEFT', '#__sted_3rd_plugin_video_gallery as vg on (vg.id = vgn.video_id)')
    ->join('LEFT', '#__sted_user_admin as admin on (admin.uid = vgn.aid)')
    ->where([
      ['vgn.chid', '=', $chid],
      ['vgn.type', '=', $type],
      ['vgn.tab', '=', $tab],
      ['vgn.rowid', '=', $item]
    ])
    ->order('dateuploaded', DESC);

    DBHelper::setQuery($query);
    $videos = DBHelper::loadAssocList();

    return $videos;
  }

}
