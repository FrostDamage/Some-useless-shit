<?php

function action_reassign()
{
    $acl = $this->checkAccess();
    if(in_array($acl, $this->aclValues, TRUE)) {

        $init_vars = [
          'id',
          'list',
        ];
        $vars = $this->importVariables($init_vars);

        if(!empty($vars->list)) {
            $query = DBHelper::getQuery(true)
            ->update('#__sted_daily_risk_actions')
            ->set([
              ['area_id', '=', $vars->list]
            ])
            ->where([
              ['area_id', '=', $vars->id]
            ]);
            DBHelper::setQuery($query);

            /* UPDATE */
            $query = DBHelper::getQuery(true)
            ->update('#__sted_daily_risk_actions_days')
            ->set([
              ['area_id', '=', $vars->list]
            ])
            ->where([
              ['area_id', '=', $vars->id]
            ]);
            DBHelper::setQuery($query);

            /* UPDATE */
            $query = DBHelper::getQuery(true)
            ->update('#__sted_daily_risk_actions_days')
            ->set([
              ['active', '=', '0']
            ])
            ->where([
              ['id', '=', $vars->id]
            ]);
            DBHelper::setQuery($query);
        }

        $this->cron_function(false);
        $result = [
          'msg' => 'Action complete',
          'complete' => true
        ];
        
        if (!empty(mysql_error())) { // if errors
            $result = [
          'msg' => 'Some errors in functions....',
          'complete' => false,
        ];
        }

        $response = [
          'result' => $result,
        ];
    } else {
        $response = [
          'result'   => 'No access. Sign in.',
          'complete' => FALSE,
    ];
    }
    $this->jsonize($response);
}
