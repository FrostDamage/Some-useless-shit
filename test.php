<?php

function getAdminById() {

  /* sw_task getAdminById
  {
    "summary":"Get admin data by id",
    "responses":"getAdminById_response",
    "parameters": [{ "schema": "getAdminById_input"}]
  }*/

  /* sw_model getAdminById_input
  {
    "properties":{
      "id":{
        "type":"integer",
        "example":12,
        "description":"id of hte admin"
      }
    }
  } */

  /* sw_model getAdminById_response
  {
    "propeties":{
      "result":{
        "type":"Object",
        "schema":"getAdminById_result"
      },
      "admin_info":{
        "type":"Object",
        "schema":"getAdminById_admin_info"
      },
      "acl":{
        "type":"Array",
        "schema":"getAdminById_acl"
      }
    }
  } */


  /* sw_model getAdminById_result
  {
    "properties":{
      "msg":{
        "type":"string",
        "example":"Action complete"
      },
      "complete":{
        "type":"boolean",
        "example":true
      }
    }
  } */

	$acl = $this->checkAccess();

	$userid = $this->jinput->get('id',  0);

	$this->options = $this->getOptionsUpdate();

  $query = DBHelper::qetQuery(true)
  ->select('id as id_value', 'title as text')
  ->from('#__sted_user_acl');

  DBHelper::setQuery($query);
  $Acl = DBHelper::loadObjectList();

	$response = [
		'result' => [
			'msg' => 'Action complete',
			'complete' => true
		],
		'admin_info' => $this->options,
		'acl' => $Acl,
	];

	$this->jsonize($response);
}
