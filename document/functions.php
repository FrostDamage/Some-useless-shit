<?php

function geturls()
{
  $string = $_SERVER['REQUEST_URI'];
  $str = urldecode($string);
  preg_match_all('|"(.*?)"|i', $str, $url);
  return $url[1];
}

function getdata($url)
{
  foreach ($url as $value)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $value);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $error = curl_error($ch);
    if ($error != "")
    {
      echo "Cannot connect to http, error " . $error;
    } else {
      return $result;
    }
  }
}

function parse_data($data)
{
  global $prices;
  preg_match_all('|<div class="price">(.*?)</div>|i', $data, $out);
  for ($i = 0; $i < count($out[1]); $i++)
  {
    $prices[] = $out[1][$i];
  }
  return $prices;
}
