<?php

/**
*	@package mobile.babysdays
*	@subpackage com_sted_mobile
*	@author Stuart Adamson
*	@version 1.0.0 28.10.2016
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');


class Sted_MobileModelCandc extends JModelList
{
	public function getPending()
	{
		$query = DBHelper::getQuery(true)
		->select(['candc.id', 'candc.type', 'candc.title', 'candc.description', 'candc.related_to', 'candc.administrator', 'candc.document_size', 'candc.folder_id',
		          'candc.added_by_parent', 'candc.added_by', 'candc.approved', 'candc.compliment', 'candc.parent_first_name', 'candc.parent_last_name', 'admin.first_name', 'admin.image'])
		->from('#__sted_candc as candc')
		->join('LEFT', '#__sted_user_admin as admin on (admin.id = candc.administrator)')
		->join('LEFT', '#__sted_user_admin as parent on (parent.uid = candc.added_by)')
		->where([
			['candc.approved', '=', '0']
		]);
		DBHelper::setQuery($query);
		$pending = DBHelper::loadAssocList();

		return $pending;
	}

	public function getCandcFolders()
	{
		$query = DBHelper::getQuery(true)
		->select(['candc.id', 'candc.folder_name', 'admin.first_name', 'admin.last_name', 'admin.image'])
		->from('#__sted_candc_folders as candc')
		->join('LEFT', '#__sted_user_admin as admin on (admin.id = candc.administrator)')
		->order('folder_name ASC');
		DBHelper::setQuery($query);
		$folders = DBHelper::loadAssocList();
		$toReturn = array();
		$counter = 1;
		foreach($folders as $folder)
		{
			$query = DBHelper::getQuery(true)
			->select(['candc.id', 'candc.type', 'candc.title', 'candc.description', 'candc.related_to', 'candc_administrator', 'candc.document_size', 'candc.folder_id', 'candc.added_by_parent', 'candc.added_by',
                'candc.approved', 'candc.compliment', 'candc.parent_first_name', 'candc.parent_last_name', 'admin.first_name', 'admin.last_name'])
			->from('#__sted_candc as candc')
			->join('LEFT', '#__sted_user_admin as parent on (parent.uid = candc.added_by)')
			->join('LEFT', '#__sted_user_admin as admin on (admin.id = candc.administrator)')
			->where([
				['candc.approved', '=', '1'],
				['folder_id', '=', $folder['id']]
			])
			->order('title ASC');
			DBHelper::setQuery($query);
			$candc = DBHelper::loadAssocList();
			$toReturn[$counter] = [
				"folder" => $folder,
				"candc" => $candc,
			];
			$counter++;
		}
		$query = DBHelper::getQuery(true)
		->select(['candc.id', 'candc.type', 'candc.title', 'candc.description', 'candc.related_to', 'candc.administrator', 'candc.document_size', 'candc.folder_id', 'candc.added_by_parent',
	            'candc.added_by', 'candc.approved', 'candc.compliment', 'candc.parent_first_name', 'candc.parent_last_name', 'admin.first_name', 'admin.last_name'])
		->from('#__sted_candc as candc')
		->join('LEFT', '#__sted_user_admin as parent on (parent.uid - candc.added_by)')
		->join('LEFT', '#__sted_user_admin as admin on (admin.id = candc.administrator)')
		->where([
			['candc.approved', '=', '1'],
		  ['folder_id', '=', '0']
		])
		->orwhere([
			['folder_id', '=', 'null']
		])
		->order('title ASC');
		DBHelper::setQuery($query);
		$toReturn[0] = [
			"folder" => array(),
			"candc" => $candc,
		];
		ksort($toReturn);

		return $toReturn;

	}

	public function getMainPageLists()
	{
		$folders = $this->getCandcFolders();
		$candcs = $this->getPending();

		$data = array();
		$data['data']['candcs'] = $folders;
		$data['data']['pending'] = $candcs;

		return $data;
	}

	private function fetchSupervisors()
	{
		$query = DBHelper::getQuery(true)
		->select(['admin.id', 'admin.uid', 'admin.first_name', 'admin.last_name', 'pos.title as positionTitle'])
		->from('#__sted_user_admin as admin')
		->join('LEFT', '#__sted_constructor_position as pos on (pos.id = admin.position)')
		->where([
			['acl', '<', '4'],
			['admin.block', '=', '0']
		])
		->order('first_name ASC', 'last_name ASC');
		DBHelper::setQuery($query);
		$supervisors = DBHelper::loadAssocList();

		return $supervisors;
	}

	private function getCandcs()
	{
		$query = DBHelper::getQuery(true)
		->select(['candc.*', 'admin.first_name', 'admin.last_name', 'admin.image'])
		->from('#__sted_candc as candc')
		->join('LEFT', '#__sted_user_admin as admin on (admin.id = candc.administrator)')
		->order('title ASC');
		DBHelper::setQuery($query);
		$serfs = DBHelper::loadAssocList();

		return $serfs;
	}

	private function getChildren()
	{
		$query = DBHelper::getQuery(true)
		->select(['a.id', 'a.first_name', 'a.last_name', 'b.title as room_name'])
		->from('#__sted_child as a')
		->join('INNER', '#__sted_constructor_room as b on (a.room = b.id)')
		->order('a.first_name ASC', 'a.last_name ASC');
		DBHelper::setQuery($query);
		$result = DBHelper::loadAssocList();

		return $result;

	}

	private function getCandc()
	{
		$init_vars = [
			'id',
		];
		$vars = $this->importVariables($init_vars);
		$query = DBHelper::getQuery(true)
		->select(['candc.id', 'candc.title', 'candc.compliment', 'candc.description', 'candc.approved', 'candc.parent_first_name', 'candc.parent_last_name', 'candc.related_to',
	            'candc.type', 'candc.path', 'candc.all_parents', 'candc.parents_view', 'candc.expires', 'candc.expires_on', 'candc.folder_id', 'admin.first_name', 'admin.last_name', 'admin.image'])
		->from('#__sted_candc as candc')
		->join('LEFT', '#__sted_user_admin as parent on (parent.uid = candc.added_by)')
		->join('LEFT', '#__sted_user_admin as admin on (admin.id = candc.administrator)')
		->where([
			['candc.id', '=', $vars->id]
		]);
		DBHelper::setQuery($query);
		$serf = DBHelper::loadAssoc();

		$query2 = DBHelper::getQuery(true)
		->select('parent_id')
		->from('#__sted_candc_parents')
		->where([
			['document_id', '=', $serf["id"]]
		]);
		DBHelper::setQuery($query2);
		$parents = DBHelper::loadAssocList();
		$temp = array();
		foreach($parents as $value)
		{
			$temp[] = $value["parent_id"];
		}

		if(!empty($temp))
		{
			$children = DBHelper::getQuery(true)
			->select(['a.id', 'a.first_name', 'a.last_name', 'b.title as room_name'])
			->from('#__sted_child as a')
			->join('INNER', '#__sted_constructor_room as b on (a.room = b.id)')
			->where([
				['a.id', 'in', join(',', $temp)]
			])
			->order('a.first_name ASC', 'a.last_name ASC'));
			DBHelper::setQuery($children);
			$children = DBHelper::loadAssocList();
			$serf["children"] = $children;
		}

		$serf["allowed_parents"] = $temp;

		return $serf;
	}

	private function listFolders()
	{
		$query = DBHelper::getQuery(true)
		->select(['id', 'folder_name'])
		->from('#__sted_candc_folders')
		->order('folder_name ASC');
		DBHelper::setQuery($query);
		$folders = DBHelper::loadAssocList();

		return $folders;
	}

	private function getFolder($folderId)
	{
		if($folderId == 0 || $folderId == null)
		{
			return "No Folder";
		}
		else
		{
			$query = DBHelper::getQuery(true)
			->select('area_of_learning')
			->from('#__sted_candc_folders')
			->where([
				['id', '=', $folderId]
			]);
			DBHelper::setQuery($query);
			$folder = DBHelper::loadAssoc();

			return $folder['folder_name'];
		}
	}

	private function file_size_total()
	{
		$sysconfig = JModel::getInstance('SysLimits', 'StedModel');
		$data = $sysconfig->getTotalCandcsSizeInSystem();

		return $data;
	}

	private function get_max_size()
	{
		$sysconfig = JModel::getInstance('SysLimits', 'StedModel');
		$data = $sysconfig->getCandcLimit();
		return $data;
	}

	public function viewCandC()
	{
		$candc = $this->getCandc();
		$supervisors = $this->fetchSupervisors();
		$folder = $this->getFolder($candc['folder_id']);
		$data = array();
		$data['data']['candc'] = $candc;
		$data['data']['folder'] = $folder;
		return $data;
	}

	public function editCandC()
	{
		$folders = $this->listFolders();
		$candc = $this->getCandc();
		$supervisors = $this->fetchSupervisors();
		$children = $this->getChildren();
		$data = array();
		$data['data']['folders'] = $folders;
		$data['data']['candc'] = $candc;
		$data['data']['supervisors'] = $supervisors;
		$data['data']['children'] = $children;


		return $data;
	}

	public function createCandC()
	{
		$supervisors = $this->fetchSupervisors();
		$children = $this->getChildren();
		$fileSize = $this->file_size_total();
		$folders = $this->listFolders();
		$maxSize = $this->get_max_size();
		$data = array();
		$data['data']['supervisors'] = $supervisors;
		$data['data']['children'] = $children;
		$data['data']['fileSize'] = $fileSize;
		$data['data']['foldes'] = $folders;
		$data['data']['maxSize'] = $maxSize;
		return $data;

	}

	public function delete()
	{
		$init_vars = [
			'id',
		];
		$vars = $this->importVariables($init_vars);
    $query = DBHelper::getQuery(true)
		->select('area_of_learning')
		->from('#__sted_candc')
		->where([
			['id', '=', $vars->id]
		]);
		DBHelper::setQuery($query);
		$data = DBHelper::loadAssoc();

		if(file_exists($data['path']))
		{
			unlink($data['path']);
		}
		if(file_exists(str_replace('/resized/','/thumb/',$data['path'])))
		{
			unlink(str_replace('/resized/','/thumb/',$data['path']));
		}
		$query = DBHelper::getQuery(true)
		->delete('#__sted_candc')
		->where([
			['id', '=', $id]
		]);
		DBHelper::setQuery($query);
		DBHelper::insertid();
	}

	public function insert()
	{
		$init_vars = [
			'compliment',
			'approved',
			'title',
			'description',
			'folder_id',
			'related',
			'admin',
			'parents',
			'all_parents',
			'parent_ids',
			'tempFile'
		];
		$vars = $this->importVariables($init_vars);

		if($vars->tempFile == '')
		{
			$type = 'txt';
		}
		else
		{
			$fileParts = explode('.',$vars->tempFile);
			$ext = array_reverse($fileParts);
			$ext = $ext[0];
			if($ext == 'pdf')
			{
				$type = 'pdf';
			}
			elseif($ext == 'doc' || $ext == 'docx')
			{
				$type = 'doc';
			}
			else
			{
				$type = 'image';
			}
		}

		$user =& JFactory::getUser();
		$uid = $user->get('id');
		$vars->all_parents = ($vars->all_parents == '1' && $vars->parents == '1') ? 1 : 0;
    $selectName = DBHelper::getQuery(true)
		->select(['first_name', 'last_name'])
		->from('#__sted_user_admin')
		->where([
			['uid', '=', $uid]
		]);
		DBHelper::setQuery($selectName);
		$name = DBHelper::loadAssoc();
		$expires = 0;
		$expires_on = '0000-00-00 00:00:00';

		if($type == 'txt')
		{
			$fileSize = 0;
		}
		else
		{
			$fileSize = (file_exists($vars->tempFile)) ? filesize($vars->tempFile) : 0;
		}
    $insert = DBHelper::getQuery(true)
		->insert('#__sted_candc')
		->columns('type', 'path', 'expires', 'expires_on', 'title', 'description', 'related_to', 'administrator', 'parents_view', 'date_added', 'document_size',
	             'all_parents', 'folder_id', 'approved', 'compliment', 'added_by', 'parent_first_name', 'parent_last_name')
		->values($type, $vars->tempFile, $expires, $expires_on, $vars->title, $vars->description, $vars->related, $vars->admin, $vars->parents, date('Y-m-d 00:00:00'), $fileSize, $vars->all_parents,
		         $vars->folder_id, $vars->folder_id, $vars->approved, $vars->compliment, $uid, $name['first_name'], $name['last_name']);
		DBHelper::setQuery($insert);

		if(DBHelper::insertid())
		{
			if($parents == 1)
			{
				$documentId = DBHelper::insertid();
				foreach($data["parent_ids"] as $key => $parentId)
				{
					$query = DBHelper::getQuery(true)
					->insert('#__sted_candc_parents')
					->columns('document_id', 'parent_id')
					->values($documentId, $parentId);
					DBHelper::setQuery($query);
					DBHelper::insertid();
				}
			}

			$this->cron_function();
			return true;
		}
		else
		{
			return false;
		}
	}

	public function update()
	{
    $init_vars = [
			'id',
			'compliment',
			'approved',
			'title',
			'description',
			'folder_id',
			'related',
			'admin',
			'parents',
			'all_parents',
			'parent_ids',
			'tempFile',
		];
		$vars = $this->importVariables($init_vars);
		$expires = 0;
		$expires_on = '0000-00-00 00:00:00';
		if(empty($vars->tempFile)))
		{
			$type = 'txt';
		}
		else
		{
			$fileParts = explode('.',$vars->tempFile);
			$ext = array_reverse($fileParts);
			$ext = $ext[0];
			if($ext == 'pdf')
			{
				$type = 'pdf';
			}
			elseif($ext == 'doc' || $ext == 'docx')
			{
				$type = 'doc';
			}
			else
			{
				$type = 'image';
			}
		}

		if($type == 'txt')
		{
			$fileSize = 0;
		}
		else
		{
			$fileSize = (file_exists($vars->tempFile)) ? filesize($vars->tempFile) : 0;
		}
		$update = DBHelper::getQuery(true)
		->update('#__sted_candc')
		->set([
			['type', '=', $type],
			['path', '=', $vars->tempFile],
			['expires', '=', $expires],
			['expires_on', '=', $expires_on],
			['title', '=', $vars->title],
			['description', '=', $vars->description],
			['related_to', '=', $vars->related],
			['administrator', '=', $vars->admin],
			['folder_id', '=', $vars->folder_id],
			['parents_view', '=', $vars->parents],
			['document_size', '=', $fileSize],
			['all_parents', '=', $vars->all_parents],
			['compliment', '=', $vars->compliment],
			['approved', '=', $vars->approved],
		])
		->where([
			['id', '=', $candcId]
		]);
		DBHelper::setQuery($update);
		DBHelper::insertid();

		if(DBHelper::insertid())
		{
      $query = DBHelper::getQuery(true)
			->delete('#__sted_candc_parents')
			->where([
				['document_id', '=', $candcId]
			]);
			DBHelper::setQuery($query);
			DBHelper::insertid();

			if($parents == 1)
			{
				foreach($data["parent_ids"] as $key => $parentId)
				{
					$query = DBHelper::getQuery(true)
					->insert('#__sted_candc_parents')
					->columns('document_id', 'parent_id')
					->values($candcId, $parentId);
					DBHelper::setQuery($query);
					DBHelper::insertid();
				}
			}
			$this->cron_function();
			return true;
		}
		else
		{
			return false;
		}

	}


	public function folders_main($limitstart, $itemsnum)
	{
		$query = DBHelper::getQuery(true)
		->select(['tFolders.id', 'tFolders.folder_name', 'tFolders.related_to', 'tFolders.description', 'tFolders.administrator', 'tFolders.parents_view', count(tDocs.id).' as documentCount', 'admin.first_name'
	            'admin.last_name', 'admin.image'])
		->from('#__sted_candc_folders as tFolders')
		->join('LEFT', '#__sted_user_admin as admin on (admin.id = tFolders.administrator)')
		->join('LEFT', '#__sted_candc as tDocs on (tDocs.folder_id = tFolders.id)')
		->group('tFolders.id')
		->order('folder_name');
		DBHelper::setQuery($query);
		$result = DBHelper::loadAssocList();
		$pagination = $this->getPaginationAPI("folders", $limitstart, $itemsnum);
		$toReturn = array();
		$toReturn['data']['folders'] = $result;
		$toReturn['data']['pagination'] = $pagination;

		return $toReturn;
	}

	public function getFolderView()
	{
		$id = jRequest::getVar('id','','','');
		$query = DBHelper::getQuery(true)
		->select(['tFolders.id', 'tFolders.folder_name', 'tFolders.related_to', 'tFolders.description', 'tFolders.administrator', 'tFolders.parents_view', 'tFolders.all_parents', 'admin.first_name',
	            'admin.last_name', 'admin.image'])
		->from('#__sted_candc_folders as tFolders')
		->join('LEFT', '#__sted_user_admin as admin on (admin.id = tFolders.administrator_)')
		->where([
			['tFolders.id', '=', $id]
		]);
		DBHelper::setQuery($query);
		$result = DBHelper::loadAssoc();
		if($result['parents_view'] == 1 && $result['all_parents'] == 0)
		{
			$query2 = DBHelper::getQuery(true)
			->select('parent_id')
			->from('#__sted_candc_folders_parents')
			->where([
				['folder_id', '=', $result["id"]]
			]);
			DBHelper::setQuery($query2);
			$parents = DBHelper::loadAssocList();
			$temp = array();
			foreach($parents as $value)
			{
				$temp[] = $value["parent_id"];
			}

			if(!empty($temp))
			{
				$children = DBHelper::getQuery(true)
				->select(['a.id', 'a.first_name', 'a.last_name', 'b.title as room_name'])
				->from('#__sted_child as a')
				->join('INNER', '#__sted_constructor_room as b on (a.room = b.id)')
				->where([
					['a.id', 'IN', join(',', $temp)]
				])
				->order('a.first_name ASC', 'a.last_name ASC');
				DBHelper::setQuery($children);
				$children = DBHelper::loadAssocList();
			}

		}
		else
		{
			$children = array();
		}
		$data = array();
		$data['folder'] = $result;
		$data['children'] = $children;

		return $data;
	}

	public function foldersAddInfo()
	{
		$supervisors = $this->fetchSupervisors();
		$children = $this->getChildren();
		$data = array();
		$data['data']['supervisors'] = $supervisors;
		$data['data']['children'] = $children;
		return $data;
	}

	public function fetchFolderForView()
	{
		$folder = $this->getFolderView();
		$data = array();
		$data['data'] = $folder;

		return $data;
	}


	private function pagination_query($task)
	{
		//if ($task=''){}
		switch ($task)
		{
			case 'folders':
			  $query = DBHelper::getQuery(true)
				->select(count('area_of_learning'))
				->from('#__sted_candc_folders')
				->where([
					['archived', '=', '0']
				]);
				break;
			case 'archived_folders':
			  $query = DBHelper::getQuery(true)
			  ->select(count('area_of_learning'))
			  ->from('#__sted_candc_folders')
			  ->where([
				  ['archived', '=', '1']
			  ]);
				break;

			/* HOME */
		}
		return $query;

	}

	private function getPaginationAPI($task, $limitstart, $itemsnum)
	{
		$query = $this->pagination_query($task);
		DBHelper::setQuery($query);
		jimport('joomla.html.pagination');
		$pageNav = new JPagination(DBHelper::loadResult(), $limitstart, $itemsnum);

		return $pageNav;
	}

	public function cron_function()
	{
		$select = DBHelper::getQuery(true)
		->select(count('area_of_learning'))
		->from('#__sted_candc')
		->where([
			['approved', '=', '0'],
			['added_by_parent', '=', '1']
		]);
		DBHelper::setQuery($select);
		$result = DBHelper::loadObject();
		$empty = false;
		$clean = false;
		if($result->counter == 0)
		{
			$empty = true;
		}
		else
		{
			$select = DBHelper::getQuery(true)
			->select(['candc.id', 'candc.compliment', 'title', 'type', 'date_added', 'parent_first_name as first_name', 'parent_last_name as last_name'])
			->from('#__sted_candc as candc')
			->where([
				['approved', '=', '0'],
				['added_by_parent', '=', '1']
			]);
			DBHelper::setQuery($select);
			$result = DBHelper::loadObjectList();
			$parents = array();
			if(!empty($result))
			{
				foreach($result as $document)
				{
					$parents[] = [
						'id' => $document->id,
					  'type' => $document->compliment,
					  'title' => $document->title,
					  "added_on" => $document->date_added,
					  "first_name" => $document->first_name,
					  "last_name" => $document->last_name
				  ];
				}
			}

			if(empty($parents))
			{

				$empty = false;
			}
		}

		$data = json_encode(["parents"=>$parents, "empty"=>$empty]);
		$select = DBHelper::getQuery(true)
		->select('data')
		->from('#__sted_homepage')
		->where([
			['section', '=', 'candc']
		]);
		DBHelper::setQuery($select);
		$result = DBHelper::insertid();
		$result = DBHelper::loadAssocList();
		if(empty($result))
		{
			$query = DBHelper::getQuery(true)
			->insert('#__sted_homepage')
			->columns('section', 'data')
			->values('candc', mysql_real_escape_string($data));
			DBHelper::setQuery($query);
		}
		else
		{
			$update = DBHelper::getQuery(true)
			->update('#__sted_homepage')
			->set([
				['data', '=', mysql_real_escape_string($data)]
			])
			->where([
				['section', '=', 'candc']
			]);
			$update = DBHelper::setQuery($update);
		}
		$result = DBHelper::insertid();

	}
}
?>
