<?php

/****
 * TODO: Refactor everything
 ****/

class Sted_MobileModelProgressIndividuals extends Sted_MobileModel
{
  public function getIndividuals($type, $chid, $type2, $filter = null, $from3 = null, $to_tmp = null)
  {
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);
    $query
      ->select(array("*"))
      ->from($db->quoteName("jos_sted_newprogress_notes_individual"))
      ->where(array("(`type` = " . strtolower($db->quote($type)) . " OR `type` = " . strtoupper($db->quote($type)) .")", "type2 = " . $db->quote($type2), "child_id = " . $db->quote($chid))  );

    if($from3 != null && $to_tmp != null) {
      $query->where(array("datestart BETWEEN " . $db->quote($from3) . " AND " . $db->quote($to_tmp)));
    }

    $query->order([$db->quoteName("key"), "datestart DESC"]);
    $db->setQuery( $query );
    $results = $db->loadObjectList();
    $toReturn = array();
    array_walk($results, function($result, $key) use (&$toReturn)
    {
      $tab = ($result->tab != "") ? $result->tab : "tab1";

      if(!isset($toReturn[$tab])) {
          $toReturn[$tab] = array();
      }

      if(!isset($toReturn[$tab][$result->progress_item])) {
          $toReturn[$tab][$result->progress_item] = array();
      }
      $temp2 = [
        'id' => $result->id,
        'gallery' => strtoupper($result->type),
        'progress_tab' => $result->tab,
        'progress_item' => $result->progress_item,
        'text' => $result->text,
        'link' => $result->link,
        'datestart' => $result->datestart,
        'dateend' => $result->dateend,
        'parent' => $result->parent,
        'is_note' => $result->is_note,
      ];
      $toReturn[$tab][$result->progress_item][] = $temp2;
    });

    return $toReturn;
  }




  //-----------


  function getSort($type, $chid) {
   /* GLOBAL PARSE FUNCTIONS */
    $individuals["0"] = $this->getIndividuals($type, $chid, 0);
    $individuals["2"] = $this->getIndividuals($type, $chid, 2);
    $individuals["4"] = $this->getIndividuals($type, $chid, 4);

    $func = function($data, $type)
    {
    $ret = $data;
    foreach ($ret as &$tabs)
    {
        foreach ($tabs as &$items)
        {
            foreach ($items as &$item)
            {
                $parsed_date = strtotime($item["datestart"]);
                $item["order_date"] = (int)$parsed_date - 1;
                $item["type"] = $type;
            }
        }
    }


    return $ret;
  };

    $individuals_new = $func($individuals['0'], 'obs');
    $individuals_new2 = $func($individuals['2'], 'nxt');
    $individuals_new3 = $func($individuals['4'], 'start');
    $model = JModelLegacy::getInstance('newprogress', 'Sted_MobileModel');


    $res_ind = $model->md_array_merge($individuals_new3,$individuals_new2,$individuals_new);
    $func = function($data)
   {
     $ret = $data;
     foreach ($ret as $k1 => $item_arr)
     {
       foreach ($item_arr as $k2 => $item_arr2)
       {
           $res_ind_tmp[$k1][$k2] = array_values($item_arr2);
       }
     }
     return $res_ind_tmp;
   };
    $res_ind = $func($res_ind);

    $func = function($data)
    {
      $individuals_new_res = array();
      $ret = $data;
      if (count($ret == 0)) return;
        foreach($ret as $tab_value_key => $tab_value) {
          foreach ($tab_value as $items_key => $items) {
            $reset_array_keys = array();
            foreach ($items as $items_r_key => $items_r)
            {
              $reset_array_keys[] = $items_r;
            }
            $reset_array_keys = array_column($reset_array_keys, 'order_date');
            array_multisort($reset_array_keys, SORT_ASC, SORT_STRING);
            $individuals_new_res[$tab_value_key][$items_key] = $reset_array_keys;
          }
        }
      return $individuals_new_res;
    };

    return $func($res_ind);
  }
}
